﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Response
{
    public class Response
    {
        public class DefaultResponse<T>
        {
            public Meta meta { get; set; }
            public T data { get; set; }
        }
		public class DefaultResponse<T1, T2> where T1 : class where T2 : class
		{
			public T1 meta { get; set; }
			public T2 data { get; set; }
		}

		public class Meta
		{
			public int errorCode { get; set; }
			public string errorMessage { get; set; }

			public Meta(int errorCode, string errorMessage)
			{
				this.errorCode = errorCode;
				this.errorMessage = errorMessage;
			}
		}
		public class SummaryMeta
		{
			public int errorCode { get; set; }
			public string errorMessage { get; set; }
			public int page { get; set; }
			public int pageSize { get; set; }
			public int totalRecords { get; set; }

			public SummaryMeta(int errorCode, string errorMessage, int page, int pageSize, int totalRecords)
			{
				this.errorCode = errorCode;
				this.errorMessage = errorMessage;
				this.page = page;
				this.pageSize = pageSize;
				this.totalRecords = totalRecords;
			}
		}
	}
	public class ResponseHelper
	{
		public static int SUCCESS_CODE = 200;
		public static string SUCCESS_MESSAGE = "success";
		public static int EXIST_CODE = 202;
		public static string EXIST_MESSAGE = "already exist";
		public static int FAIL_CODE = 201;
		public static string FAIL_MESSAGE = "fail";
		public static int INTERNAL_SERVER_ERROR_CODE = 500;
		public static string INTERNAL_SERVER_ERROR_MESSAGE = "internal server error";
		public static int BAD_REQUEST_CODE = 400;
		public static string BAD_REQUEST_MESSAGE = "bad request";
		public static int NOT_FOUND_CODE = 404;
		public static string NOT_FOUND_MESSAGE = "not found";
		public static int BAD_INPUT_CODE = 210;
		public static string BAD_INPUT_MESSAGE = "bad / invalid input";
		public static int USER_WRONG_PASSWORD_CODE = 211;
		public static string USER_WRONG_PASSWORD_MESSAGE = "wrong password";
	}

}
