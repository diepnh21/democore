﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utils
{
    public class Utils
    {
        public static string GetMD5Hash(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            return Convert.ToBase64String(bs);
        }

        public static string HidePhone(string phone)
        {
            if (!string.IsNullOrEmpty(phone))
                phone = phone.Remove(2, 8).Insert(2, "xxxxxx");
            return phone;
        }
    }
}
