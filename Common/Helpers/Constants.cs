﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Helpers
{
    public class Constants
    {
        public static string CONSTANTS_VERSION_JS = "0.1";

        #region User
        public static string USERS_GET_ALL_ENDPOINT = "users/get_all";
        public static string USERS_GET_BY_ID_ENDPOINT = "users/{0}";
        public static string LOGIN_ENDPOINT = "users/login";
        public static string USERS_ADD_EDIT_ENDPOINT = "users/add_and_edit";
        #endregion
    }
}
