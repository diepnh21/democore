﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public class Lib
    {
        public static DateTime FirstDayOfMonth(DateTime date)
        {
            var startOfMonth = new DateTime(date.Year, date.Month, 1);
            return startOfMonth;
        }
        public static DateTime LastDayOfMonth(DateTime date)
        {
            var startOfMonth = new DateTime(date.Year, date.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddDays(-1);
            return endOfMonth;
        }
    }
}
