﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common.Extensions
{
    public static class ConvertExtensions
    {
        // transform object into Identity data type (integer).
        public static int AsId(this object item, int defaultId = -1)
        {
            if (item == null)
                return defaultId;

            int result;
            if (!int.TryParse(item.ToString(), out result))
                return defaultId;

            return result;
        }

        // transform object into integer data type.
        public static int AsInt(this object item, int defaultInt = default(int))
        {
            if (item == null)
                return defaultInt;

            int result;
            if (!int.TryParse(item.ToString(), out result))
                return defaultInt;

            return result;
        }

        // transform object into double data type
        public static double AsDouble(this object item, double defaultDouble = default(double))
        {
            if (item == null)
                return defaultDouble;

            double result;
            if (!double.TryParse(item.ToString(), out result))
                return defaultDouble;

            return result;
        }

        public static decimal AsDecimail(this object item, decimal defaultDecimal = default(decimal))
        {
            if (item == null)
                return defaultDecimal;

            decimal result;
            if (!decimal.TryParse(item.ToString(), out result))
                return defaultDecimal;

            return result;
        }

        public static byte AsByte(this object item, byte defaultByte = default(byte))
        {
            if (item == null)
                return defaultByte;

            byte result;
            if (!byte.TryParse(item.ToString(), out result))
                return defaultByte;

            return result;
        }
        // transform object into string data type

        public static string AsString(this object item, string defaultString = default(string))
        {
            if (item == null || item.Equals(System.DBNull.Value))
                return defaultString;

            return item.ToString().Trim();
        }

        // transform object into DateTime data type.
        public static DateTime AsDateTime(this object item, DateTime defaultDateTime = default(DateTime))
        {
            if (item == null || string.IsNullOrEmpty(item.ToString()))
                return defaultDateTime;

            DateTime result;
            if (!DateTime.TryParse(item.ToString(), out result))
                return defaultDateTime;

            return result;
        }

        // transform object into bool data type
        public static bool AsBool(this object item, bool defaultBool = default(bool))
        {
            if (item == null)
                return defaultBool;

            return new List<string>() { "yes", "y", "true" }.Contains(item.ToString().ToLower());
        }

        // transform string into byte array
        public static byte[] AsByteArray(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;

            return Convert.FromBase64String(s);
        }

        // transform object into base64 string.
        public static string AsBase64String(this object item)
        {
            if (item == null)
                return null;
            return Convert.ToBase64String((byte[])item);
        }

        // transform object into Guid data type
        public static Guid AsGuid(this object item)
        {
            try { return new Guid(item.ToString()); }
            catch { return Guid.Empty; }
        }

        public static string AsToLower(this string item, string defaultString = default(string))
        {
            if (item == null || item.Equals(System.DBNull.Value))
                return defaultString;

            return item.Trim().ToLower();
        }

        public static bool ValidPhone(this string txtPhone)
        {
            if ((txtPhone.StartsWith("09")) && txtPhone.Length == 10)
                return true;

            if ((txtPhone.StartsWith("03") || txtPhone.StartsWith("08") || txtPhone.StartsWith("07") || txtPhone.StartsWith("05")) && txtPhone.Length == 10)
                return true;
            if ((txtPhone.StartsWith("3") || txtPhone.StartsWith("8") || txtPhone.StartsWith("7") || txtPhone.StartsWith("5")) && txtPhone.Length == 9)
            {
                return true;
            }
            if (txtPhone.StartsWith("9") && txtPhone.Length == 9)
            {
                return true;
            }
            if (txtPhone.StartsWith("01") && txtPhone.Length == 11)
            {
                return true;
            }
            return false;
        }

        public static long RoundingTo(long myNum, long roundTo)
        {
            if (roundTo <= 0) return myNum;
            return (myNum + roundTo / 2) / roundTo * roundTo;
        }
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var a = unixTimeStamp.ToString().Length;
            string b = unixTimeStamp.ToString().Substring(0, (a - 3));
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(b)).ToLocalTime();
            return dtDateTime;
        }

        public static string ReduceWhitespace(this string value)
        {
            try
            {
                if (value == null) return value;
                value = value.Trim();
                var newString = new StringBuilder();
                bool previousIsWhitespace = false;
                for (int i = 0; i < value.Length; i++)
                {
                    if (Char.IsWhiteSpace(value[i]))
                    {
                        if (previousIsWhitespace)
                        {
                            continue;
                        }

                        previousIsWhitespace = true;
                    }
                    else
                    {
                        previousIsWhitespace = false;
                    }

                    newString.Append(value[i]);
                }

                return newString.ToString();
            }
            catch
            {
                return value;
            }

        }

        public static string TitleCaseString(this string s)
        {
            try
            {
                if (s == null) return s;

                String[] words = s.Split(' ');
                for (int i = 0; i < words.Length; i++)
                {
                    if (words[i].Length == 0) continue;

                    Char firstChar = Char.ToUpper(words[i][0]);
                    String rest = "";
                    if (words[i].Length > 1)
                    {
                        rest = words[i].Substring(1).ToLower();
                    }
                    words[i] = firstChar + rest;
                }
                return String.Join(" ", words);
            }
            catch
            {
                return s;
            }

        }
        public static bool IsValidUri(this string url)
        {
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(url);
        }
        //Check IsNumber
        public static bool IsNumber(string pValue)
        {
            foreach (Char c in pValue)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
    }
}
