﻿using DAL.EntityFramework;
using DAL.Respository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly DemoContext context;
        private IDbContextTransaction _dbContextTransaction;

        public IGenericRepository<User> _userRepository;

        public UnitOfWork(DemoContext context)
        {
            this.context = context;
        }
        public IGenericRepository<User> UserRepository
        {
            get { return _userRepository ?? (_userRepository = new GenericRepository<User>(context)); }
        }
        public void BeginTransaction()
        {
            _dbContextTransaction = context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            SaveChanges();
            try
            {
                SaveChanges();
                _dbContextTransaction.Commit();
            }
            finally
            {
                _dbContextTransaction.Dispose();
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                    if (_dbContextTransaction != null)
                        _dbContextTransaction.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public DemoContext GetContext()
        {
            return context;
        }

        public void RollbackTransaction()
        {
            _dbContextTransaction.Rollback();
            _dbContextTransaction.Dispose();
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void SaveChanges()
        {
            var modifiedEntries = context.ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);
            foreach (var entity in modifiedEntries)
            {
                //((EntityInformation)entity.Entity).ModifiedDate = DateTime.Now;
                //((EntityInformation)entity.Entity).ModifiedUserName = userName;
            }
            context.SaveChanges();
        }
    }
}
