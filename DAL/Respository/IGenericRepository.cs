﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Respository
{
	public interface IGenericRepository<TEntity>
	{
		/// <summary>
		/// Get all entities from db
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="orderBy"></param>
		/// <param name="includes"></param>
		/// <returns></returns>
		List<TEntity> Get(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool tracking = false,
			params Expression<Func<TEntity, object>>[] includes);

		/// <summary>
		/// Get all entities from db by page
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="orderBy"></param>
		/// <param name="includes"></param>
		/// <param name="page"></param>
		/// <param name="pagSize"></param>
		/// <returns></returns>
		List<TEntity> Get(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int page = 1, int pageSize = 50, bool tracking = false,
			params Expression<Func<TEntity, object>>[] includes);

		/// <summary>
		/// Get query for entity
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="orderBy"></param>
		/// <returns></returns>
		IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool tracking = false, params Expression<Func<TEntity, object>>[] includes);

		/// <summary>
		/// Get query for entity
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="orderBy"></param>
		/// <param name="page"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int page = 1, int pageSize = 50, bool tracking = false, params Expression<Func<TEntity, object>>[] includes);

		/// <summary>
		/// Reload single entity
		/// </summary>
		/// <param name="entity"></param>
		/// <returns></returns>
		void Reload(TEntity entity);

		/// <summary>
		/// Get single entity by primary key
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		TEntity GetById(object id);

		/// <summary>
		/// Get first or default entity by filter
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="includes"></param>
		/// <returns></returns>
		TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> filter = null, bool tracking = false, params Expression<Func<TEntity, object>>[] includes);

		/// <summary>
		/// Insert entity to db
		/// </summary>
		/// <param name="entity"></param>
		TEntity Insert(TEntity entity);
		/// <summary>
		/// Insert list entity to db
		/// </summary>
		/// <param name="entities"></param>
		/// <returns></returns>
		/// 
		void SingleInsert(TEntity entity);
		/// <summary>
		/// Insert list entity to db
		/// </summary>
		/// <param name="entities"></param>
		/// <returns></returns>
		List<TEntity> Insert(List<TEntity> entities);

		/// <summary>
		/// Update entity in db
		/// </summary>
		/// <param name="entity"></param>
		void Update(TEntity entity);

		/// <summary>
		/// Update multiple entity in db
		/// </summary>
		/// <param name="filter"></param>
		/// <param name="update"></param>
		int Update(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, TEntity>> update = null);

		void UpdateNotRelationship(TEntity entity);
		/// <summary>
		/// Delete entity from db by primary key
		/// </summary>
		/// <param name="id"></param>
		void Delete(object id);

		/// <summary>
		/// update property IsDeleted = true
		/// </summary>
		/// <param name="id"></param>
		void SoftDelete(object id);

		/// <summary>
		/// Delete multiple entity from db
		/// </summary>		
		/// <param name="filter"></param>		
		int Delete(Expression<Func<TEntity, bool>> filter = null);

		/// <summary>
		/// Get all entity from db
		/// </summary>		
		IQueryable<TEntity> All(bool tracking = false);

		/// <summary>
		/// Get any by filter
		/// </summary>		
		bool Any(Expression<Func<TEntity, bool>> filter = null);
		/// <summary>
		/// Count entity by filter
		/// </summary>
		/// <param name="filter"></param>
		/// <returns></returns>
		int Count(Expression<Func<TEntity, bool>> filter = null);
	}
}
