﻿using DAL.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace DAL.Respository
{
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
	{
		private DbContext context;
		private DbSet<TEntity> dbSet;

		public GenericRepository(DbContext context)
		{
			this.context = context;
			this.dbSet = context.Set<TEntity>();
		}
		public virtual List<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool tracking = false, params Expression<Func<TEntity, object>>[] includes)
		{
			IQueryable<TEntity> query = tracking ? dbSet : dbSet.AsNoTracking();

			foreach (Expression<Func<TEntity, object>> include in includes)
				query = query.Include(include);

			if (filter != null)
				query = query.Where(filter);

			if (orderBy != null)
				query = orderBy(query);

			return query.ToList();
		}

		public virtual List<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int page = 1, int pageSize = 50, bool tracking = false, params Expression<Func<TEntity, object>>[] includes)
		{
			IQueryable<TEntity> query = tracking ? dbSet : dbSet.AsNoTracking();

			foreach (Expression<Func<TEntity, object>> include in includes)
				query = query.Include(include);

			if (filter != null)
				query = query.Where(filter);

			if (orderBy != null)
				query = orderBy(query);

			return query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
		}

		public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool tracking = false, params Expression<Func<TEntity, object>>[] includes)
		{
			IQueryable<TEntity> query = tracking ? dbSet : dbSet.AsNoTracking();

			foreach (Expression<Func<TEntity, object>> include in includes)
				query = query.Include(include);

			if (filter != null)
				query = query.Where(filter);

			if (orderBy != null)
				query = orderBy(query);

			return query;
		}

		public virtual IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, int page = 1, int pageSize = 50, bool tracking = false, params Expression<Func<TEntity, object>>[] includes)
		{
			IQueryable<TEntity> query = tracking ? dbSet : dbSet.AsNoTracking();

			foreach (Expression<Func<TEntity, object>> include in includes)
				query = query.Include(include);

			if (filter != null)
				query = query.Where(filter);

			if (orderBy != null)
				query = orderBy(query);

			return query.Skip((page - 1) * pageSize).Take(pageSize);
		}

		public virtual TEntity GetById(object id)
		{
			return dbSet.Find(id);
		}

		public virtual void Reload(TEntity entity)
		{
			context.Entry(entity).Reload();
		}

		public virtual TEntity GetFirstOrDefault(Expression<Func<TEntity, bool>> filter = null, bool tracking = false, params Expression<Func<TEntity, object>>[] includes)
		{
			IQueryable<TEntity> query = tracking ? dbSet : dbSet.AsNoTracking();

			foreach (Expression<Func<TEntity, object>> include in includes)
				query = query.Include(include);

			return query.FirstOrDefault(filter);
		}

		public virtual TEntity Insert(TEntity entity)
		{
			dbSet.Add(entity);
			return entity;
		}

		public virtual void SingleInsert(TEntity entity)
		{
			dbSet.SingleInsert(entity);
		}

		public virtual void Update(TEntity entity)
		{
			dbSet.Attach(entity);
			context.Entry(entity).State = EntityState.Modified;
		}

		public virtual void Delete(object id)
		{
			TEntity entityToDelete = dbSet.Find(id);
			if (context.Entry(entityToDelete).State == EntityState.Detached)
			{
				dbSet.Attach(entityToDelete);
			}
			dbSet.Remove(entityToDelete);
		}

		public virtual void SoftDelete(object id)
		{
			TEntity entityToDelete = dbSet.Find(id);
			if (entityToDelete.HasProperty("IsDeleted"))
			{
				entityToDelete.SetProperty("IsDeleted", true);
				dbSet.Attach(entityToDelete);
				context.Entry(entityToDelete).State = EntityState.Modified;
			}
		}

		public virtual IQueryable<TEntity> All(bool tracking = false)
		{
			return tracking ? dbSet : dbSet.AsNoTracking();
		}

		public int Update(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, TEntity>> update = null)
		{
			int affectedRowCount = dbSet.Where(filter).Update(update);
			return affectedRowCount;
		}

		public virtual void UpdateNotRelationship(TEntity entity)
		{
			dbSet.Attach(entity);
			var entry = context.Entry(entity);
			entry.State = EntityState.Modified;
			context.Entry(entity).State = EntityState.Modified;
			var modifiedEntries = context.ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);
			foreach (var item in modifiedEntries)
			{
				if (item.Entity as TEntity != null)
					item.State = EntityState.Modified;
				else
					item.State = EntityState.Unchanged;
			}

		}

		public int Delete(Expression<Func<TEntity, bool>> filter = null)
		{
			var rows = dbSet.Where(filter).ToList();
			foreach (var entityToDelete in rows)
			{
				if (context.Entry(entityToDelete).State == EntityState.Detached)
				{
					dbSet.Attach(entityToDelete);
				}
				dbSet.Remove(entityToDelete);
			}
			return rows.Count();
		}

		public bool Any(Expression<Func<TEntity, bool>> filter = null)
		{
			return dbSet.Any(filter);
		}

		public int Count(Expression<Func<TEntity, bool>> filter = null)
		{
			return dbSet.Count(filter);
		}

		public List<TEntity> Insert(List<TEntity> entities)
		{
			foreach (var entity in entities)
				dbSet.Add(entity);
			return entities;
		}
	}
}
