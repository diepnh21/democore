﻿using System;
using System.Collections.Generic;

namespace DAL.EntityFramework
{
    public partial class User
    {
        public int? Id { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Email { get; set; }
        public string? FullName { get; set; }
        public int? Status { get; set; }
        public DateTime? Birthday { get; set; }
        public string? Token { get; set; }
    }
}
