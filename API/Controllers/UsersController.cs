﻿using Common.Response;
using DAL.EntityFramework;
using DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using static Common.Response.Response;

namespace API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private IConfiguration _configuration;

        public UsersController(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("search")]
        //[Authorize]
        public ActionResult<DefaultResponse<SummaryMeta, List<User>>> Search([FromQuery] int page = 1, [FromQuery] int pageSize = 10, [FromQuery] int skip = -1,
            [FromQuery] int take = 20, [FromQuery] string search = null, [FromQuery] int status = -1)
        {
            var def = new DefaultResponse<SummaryMeta, List<User>>();
            try
            {
                var query = _unitOfWork.UserRepository.Query(x => (x.Status == status || status == -1)
                                                                && (x.Username.Contains(search) || search == null), null, false).ToList();
                var totalRecords = query.Count();
                List<User> data;
                if (skip == -1)
                    data = query.OrderByDescending(x => x.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
                else
                {
                    data = query.Skip(skip).Take(take).ToList();
                    pageSize = take;
                }
                def.meta = new SummaryMeta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE, page, pageSize, totalRecords);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("get_all")]
        public ActionResult<DefaultResponse<Meta, List<User>>> GetAll()
        {
            var def = new DefaultResponse<Meta, List<User>>();
            try
            {
                var data = _unitOfWork.UserRepository.Query(x => x.Status == 1, null, false).ToList();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
            }
            catch (Exception)
            {

                throw;
            }
            return Ok(def);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<DefaultResponse<Meta, User>>> GetById(int id)
        {
            var def = new DefaultResponse<Meta, User>();
            try
            {
                var data =  await _unitOfWork.UserRepository.Query(x => x.Id == id
                , null, false).FirstOrDefaultAsync();

                def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                def.data = data;
                return Ok(def);
            }
            catch (Exception ex)
            {

            }
            return Ok(def);
        }

        [HttpPost]
        [Route("add_and_edit")]
        public ActionResult<DefaultResponse<object>> AddAndEdit([FromBody] User entity)
        {
            var def = new DefaultResponse<object>();
            try
            {
                if (string.IsNullOrEmpty(entity.Username) || string.IsNullOrEmpty(entity.Password))
                {
                    def.meta = new Meta(ResponseHelper.BAD_INPUT_CODE, ResponseHelper.BAD_INPUT_MESSAGE);
                    return Ok(def);
                }
                entity.Username = entity.Username.Trim().ToLower();
                //Đã tồn tại
                if (entity.Id > 0)
                {
                    var info = _unitOfWork.UserRepository.Query(x => x.Id == entity.Id, null, false).FirstOrDefault();
                    if (info != null)
                    {
                        info.Username = entity.Username;
                        info.Password = entity.Password;
                        info.FullName = entity.FullName;
                        info.Email = entity.Email;
                        
                        _unitOfWork.UserRepository.Update(info);

                        def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                        def.data = entity.Id;
                        _unitOfWork.Save();
                    }
                }
                else
                {
                    //Kiem tra xem user da ton tai hay chua
                    var check = _unitOfWork.UserRepository.Query(x => x.Username == entity.Username, null, false).FirstOrDefault();
                    if (check != null)
                    {
                        def.meta = new Meta(ResponseHelper.EXIST_CODE, ResponseHelper.EXIST_MESSAGE);
                        return Ok(def);
                    }
                    //Insert
                    var result = _unitOfWork.UserRepository.Insert(entity);
                    def.meta = new Meta(ResponseHelper.SUCCESS_CODE, ResponseHelper.SUCCESS_MESSAGE);
                    _unitOfWork.Save();
                    def.data = result.Id;
                }



            }
            catch (Exception ex)
            {
                def.meta = new Meta(ResponseHelper.INTERNAL_SERVER_ERROR_CODE, ResponseHelper.INTERNAL_SERVER_ERROR_MESSAGE);
            }
            return Ok(def);
        }
    }
}
