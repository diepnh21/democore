﻿using DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.Users
{
    public interface IUserServices
    {
        Task<User> Login(string access_token, User entity);
        Task<List<User>> GetAll(string token);
        Task<User> GetById(string token,int id);
        Task<int> AddAndEdit(string token,User entity);
    }
}
