﻿using Common.Helpers;
using DAL.EntityFramework;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Common.Response.Response;

namespace Services.Services.Users
{
    public class UserServices : BaseServices, IUserServices
    {
        public UserServices(IConfiguration configuration, IHttpClientFactory clientFactory) : base(configuration, clientFactory)
        {
            _baseConfig = configuration;
        }
        public async Task<int> AddAndEdit(string token,User entity)
        {
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<dynamic>>(Constants.USERS_ADD_EDIT_ENDPOINT, token, stringContent);
                if (response.meta.errorCode == 200)
                    return (int)response.data;
            }
            catch (Exception ex)
            {

            }
            return 0;
        }

        public async Task<List<User>> GetAll(string token)
        {
            var result = new List<User>();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, List<User>>>(string.Format(Constants.USERS_GET_ALL_ENDPOINT), token);
                if (response.meta.errorCode == 200)
                    result = response.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public async Task<User> GetById(string token,int id)
        {
            var result = new User();
            try
            {
                var response = await GetAsync<DefaultResponse<Meta, User>>(string.Format(Constants.USERS_GET_BY_ID_ENDPOINT, id), token);
                if (response.meta.errorCode == 200)
                    result = response.data;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public async Task<User> Login(string access_token, User entity)
        {
            var data = new User();
            try
            {
                var json = JsonConvert.SerializeObject(entity);
                var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
                var response = await PostAsync<DefaultResponse<User>>(Constants.LOGIN_ENDPOINT, access_token, stringContent);
                if (response.meta.errorCode == 200)
                    return response.data;
            }
            catch (Exception ex)
            {

            }
            return data;
        }
    }
}
