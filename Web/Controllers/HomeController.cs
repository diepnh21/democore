﻿using Microsoft.AspNetCore.Mvc;
using Services.Services.Users;
using System.Diagnostics;
using Web.Helpers;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        private IConfiguration _configuration;
        private readonly IUserServices _userService;

        public HomeController(IConfiguration configuration, IUserServices userService) : base(configuration)
        {
            _configuration = configuration;
            _userService = userService;
        }

        [PermissionFilter]
        public IActionResult Index()
        {
            var data = _userService.GetAll("");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}