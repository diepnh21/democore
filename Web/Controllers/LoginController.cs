﻿using Common.Helpers;
using Common.Utils;
using DAL.EntityFramework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Services.Users;
using Web.Helpers;
using static Common.Response.Response;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUserServices _userService;

        public LoginController(IUserServices userService)
        {
            _userService = userService;
        }

        [Route("login.html")]
        [Route("/")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PostLogin(User obj)
        {
            try
            {
                var token = "";
                var loginReq = new User()
                {
                    Username = obj.Username,
                    Password = Utils.GetMD5Hash(obj.Password)
                };
                var data = await _userService.Login(token, loginReq);
                if(data != null)
                {
                    if (data.Status == 0)// tk bị khóa
                        return Json(new { status = 0, message = "Tài khoản đã bị khóa" });

                    // save access token					
                    HttpContext.Session.SetObjectAsJson("USER_DATA", data);
                    await HttpContext.Session.CommitAsync();

                    //return Redirect(result.data.Group.DefaultPath);
                    return Json(new { status = 1, message = "" });
                }
                else
                    return Json(new { status = 0, message = "Thông tin đăng nhập không đúng, vui lòng thử lại!" });   

            }

            catch (Exception ex)
            {
                return Json(new { status = 0, message = "Xảy ra lỗi khi đăng nhập. Vui lòng thử lại." });
            }
        }

        [Route("logout.html")]
        public IActionResult Logout()
        {
            HttpContext.Session.SetObjectAsJson("USER_DATA", null);
            return Redirect("/");
        }
    }
}
