﻿using DAL.EntityFramework;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Web.Helpers;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected IConfiguration _baseConfig;
        public BaseController(IConfiguration configuration)
        {
            _baseConfig = configuration;
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var user = GetUserGlobal();
            if (user == null || string.IsNullOrEmpty(user.Token))
                filterContext.Result = new RedirectResult("/");

            base.OnActionExecuted(filterContext);
        }
        protected string GetToken()
        {
            return HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result.Token;
        }
        protected User GetUserGlobal()
        {
            return HttpContext.Session.GetObjectFromJson<User>("USER_DATA").Result;
        }

        public virtual object GetBaseObjectResult(bool isSuccess = true, string msg = "", object data = null, string html = "")
        {
            return new
            {
                IsSuccess = isSuccess,
                Message = msg,
                Data = data,
                Html = html
            };
        }
    }
}
